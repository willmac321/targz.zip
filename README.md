Dont click random links?



## build tools
vite, react, firebase, eslint, prettier, flask, python


```
npm i
npm run dev
```

### to deploy
```
// install firebase and login then
npm run build
firebase deploy
```
# About
Stack used is Flask, React, Vite, prettier, eslint, python Firebase Functions/Storage/Hosting

I made a few functions and wrapped them in a flask app, the functions save images to storage, pull those images from storage and modify them, and then also returns signed links to download and delete images.

Also using Gromment as a UI component library

# Deploy
```
firebase deploy
```

#Emulate
if you want to run the built version of frontend simply use
```
firebase emulate:start
```

to run the dev frontend and emulate only storage and the functions
```
// in web
npm run dev

// also 
firebase emulate:start --only storage,functions
