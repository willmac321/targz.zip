from datetime import datetime, timedelta, timezone

from firebase_admin import storage

# Define how old files need to be before deletion (e.g., 7 days)
DAYS_OLD = 2


def auto_delete_old_files():
    # Initialize the Firebase Storage bucket
    bucket = storage.bucket()

    # Get all files in the Firebase Storage bucket
    blobs = bucket.list_blobs(prefix="igformat/")

    # Get the current time and calculate the cutoff time
    cutoff_time = datetime.now(timezone.utc) - timedelta(days=DAYS_OLD)

    for blob in blobs:
        # Check if the file has the uploadTimestamp in metadata
        last_modified = blob.updated
        # If the file is older than the cutoff time, delete it
        if last_modified < cutoff_time:
            print(f"Deleting file: {blob.name}, last changed at {last_modified}")
            blob.delete()

    return "Completed auto-delete task"
