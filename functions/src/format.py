""" reshape the image, reshape the world """

import io
import tempfile

from PIL import Image, ImageOps
from werkzeug.datastructures import FileStorage


def is_valid_file(file) -> bool:
    """
    Check if the file is a valid image file.
    """
    try:
        with Image.open(file) as img:
            img.verify()
        return True
    except (IOError, SyntaxError):
        return False


class Formatter:
    """
    A class to format an image by resizing, adding borders, and framing.
    """

    # Class attributes
    max_dim: int = 2560
    border_width: int = 20
    frame_color: str = "#ffffff"
    background_color: str = "#000000"
    new_image: Image.Image
    filepath: str

    def __init__(self, file: FileStorage | str, filename: str | None = None) -> None:
        """
        Initialize the Formatter with a file and validate it.
        """
        self.new_picture_width: float = 0.0
        self.new_picture_height: float = 0.0
        self.image_ratio: float = 0.0

        if is_valid_file(file):
            with Image.open(file) as img:
                ImageOps.exif_transpose(img, in_place=True)
                self.new_image = img.copy()
        if not filename and type(file) is FileStorage:
            self.filename = file.filename
        else:
            self.filename = filename

    def save(self):
        """Save the new image to a BytesIO object."""
        if not self.filename or not self.new_image:
            return
        in_mem_file = io.BytesIO()
        file_extension = self.filename.rsplit(".", 1)[-1].upper()
        if file_extension == "JPG":
            file_extension = "JPEG"

        with tempfile.NamedTemporaryFile(
            delete=False, suffix=file_extension
        ) as in_mem_file:
            self.new_image.save(in_mem_file, format=file_extension)
            self.filepath = in_mem_file.name

    def set_background_color(self, color: str) -> None:
        """Set the background color for the frame."""
        self.background_color = color

    def set_frame_color(self, color: str) -> None:
        """Set the frame color for the border."""
        self.frame_color = color

    def set_max_size(self, n: int) -> None:
        """Set the maximum size of the output image."""
        self.max_dim = n

    def set_border_width(self, n: int) -> None:
        """Set the border width in pixels."""
        self.border_width = n

    def set_picture_dims(self) -> None:
        """
        Calculate and set the new dimensions of the picture while maintaining the aspect ratio.
        """
        layer_width = self.new_image.width
        layer_height = self.new_image.height
        self.image_ratio = layer_width / layer_height

        if layer_width <= layer_height:
            self.new_picture_width = (self.max_dim * self.image_ratio) - (
                2 * self.border_width
            )
        else:
            self.new_picture_width = self.max_dim - (2 * self.border_width)

        if layer_height < layer_width:
            self.new_picture_height = (self.max_dim / self.image_ratio) - (
                2 * self.border_width
            )
        else:
            self.new_picture_height = self.max_dim - (2 * self.border_width)

    def resize(self) -> None:
        """Resize the image to the calculated dimensions."""
        self.new_image = self.new_image.resize(
            (int(self.new_picture_width), int(self.new_picture_height))
        )

    def frame(self) -> None:
        """Add a frame and background to the image."""
        background = Image.new(
            "RGB", (self.max_dim, self.max_dim), self.background_color
        )

        frame = Image.new(
            "RGB",
            (
                int(self.new_picture_width + 2 * self.border_width),
                int(self.new_picture_height + 2 * self.border_width),
            ),
            self.frame_color,
        )

        bg_w, bg_h = background.size
        frame_offset = (
            (bg_w - frame.width) // 2,
            (bg_h - frame.height) // 2,
        )
        img_offset = (
            (bg_w - int(self.new_picture_width)) // 2,
            (bg_h - int(self.new_picture_height)) // 2,
        )

        background.paste(frame, frame_offset)
        background.paste(self.new_image, img_offset)
        self.new_image = background

    def save_new_image_name(self) -> None:
        """
        Save the processed image, overwriting files with the same name.
        Returns the saved file path and new file name.
        """
        if self.new_image is not None and self.filename is not None:
            basename, file_ext = self.filename.split(".")
            self.filename = f"{basename}_IG.{file_ext}"
            self.save()

    def resize_and_frame(self) -> None:
        """Resize the image and add a frame."""
        self.set_picture_dims()
        self.resize()
        self.frame()
