from datetime import datetime, timedelta

from firebase_admin import storage


def get_media_signed_url(filename, directory):
    bucket = storage.bucket()
    blob = bucket.blob(f'{directory}/{filename}')
    return blob.generate_signed_url(datetime.now() + timedelta(hours=24))
