from datetime import datetime, timedelta

from firebase_admin import storage


def add_pictures(images):
    bucket = storage.bucket()
    for image in images:
        blob = bucket.blob(f"igformat/{image['filename']}")
        with open(image["filepath"], "rb") as filepath:
            blob.upload_from_file(filepath)


def delete_picture(filename):
    bucket = storage.bucket()
    bucket.delete_blob(f"igformat/{filename}")


def add_pictures_no_filenames(image, filename):
    bucket = storage.bucket()
    blob = bucket.blob(f"igformat/{filename}")
    blob.upload_from_filename(image)
    return blob.generate_signed_url(datetime.now() + timedelta(minutes=10))


def get_picture_from_storage(filename, tmp_dir):
    bucket = storage.bucket()
    blob = bucket.blob(f"igformat/{filename}")
    blob.download_to_filename(f"{tmp_dir}/{filename}")
    return f"{tmp_dir}/{filename}"
