#!/usr/bin/env python3
"""
Module Docstring
"""
import os
import tempfile

import requests
from firebase_admin import credentials, initialize_app
from firebase_functions import https_fn, options, scheduler_fn
from flask import Flask, Response, jsonify, request
from requests import status_codes

from src.download import get_media_signed_url
from src.format import Formatter, is_valid_file
from src.scripts import sweep
from src.upload import (add_pictures, add_pictures_no_filenames,
                        delete_picture, get_picture_from_storage)

__author__ = "wmac"
__version__ = "0.1.0"
__license__ = "MIT"

initialize_app(
    credentials.Certificate(
        "./firebase/targz-zip-firebase-adminsdk-srxjc-97f647c8c1.json"
    ),
    {"storageBucket": "targz-zip.appspot.com"},
)


# run every 3 months, in case billing
@scheduler_fn.on_schedule(schedule="0 0 1 1,4,7,10 *")
def scheduled_deletion(_: scheduler_fn.ScheduledEvent) -> None:
    """sweep"""
    sweep.auto_delete_old_files()


app = Flask(__name__)

# 75MB max length
app.config["MAX_CONTENT_LENGTH"] = 75 * 1000 * 1000


@app.route("/api/processImages", methods=["POST"])
def upload_images():
    files = request.files.getlist("file")
    if not files:
        return Response(status=requests.codes.bad)
    formatted_files = []
    for file in files:
        formatter = Formatter(file)
        if not is_valid_file(file):
            return Response(status=requests.codes.bad)

        formatter.set_picture_dims()
        formatter.resize()
        formatter.save()

        formatted_files.append(
            {"filepath": formatter.filepath, "filename": formatter.filename}
        )

    add_pictures(formatted_files)

    for file in formatted_files:
        os.remove(file["filepath"])

    return Response(status=requests.codes.ok)


@app.route("/api/formatImages", methods=["POST"])
def format_images():
    rv = []
    with tempfile.TemporaryDirectory() as tmp_dir:
        data = request.get_json()
        for file_name in data.get("files"):
            file = get_picture_from_storage(file_name, tmp_dir)
            formatter = Formatter(file, file_name)
            if is_valid_file(file):
                formatter.set_max_size(data.get("max_size", 1280))
                formatter.set_border_width(data.get("border_width", 20))
                formatter.set_background_color(data.get("background_color", "#000000"))
                formatter.set_frame_color(data.get("frame_color", "#FFFFFF"))
                formatter.resize_and_frame()
                formatter.save_new_image_name()
                fp, fn = formatter.filepath, formatter.filename
                if fp is not None:
                    url = add_pictures_no_filenames(fp, fn)
                    rv.append([fn, url])
                    delete_picture(file_name)

    return jsonify(rv), 200


@app.route("/api/removeImages", methods=["DELETE"])
def delete_images():
    files = request.args.getlist("files[]")
    for name in files:
        delete_picture(name)
    return Response(status=status_codes.codes.OK)


@app.route("/api/media", methods=["GET"])
def get_media():
    file = request.args.get("file")
    directory = request.args.get("directory")
    rv = get_media_signed_url(file, directory)
    return jsonify(rv), 200


@https_fn.on_request(
    max_instances=2,
    cors=options.CorsOptions(
        cors_origins=["*"], cors_methods=["delete", "post", "get"]
    ),
)
def api(req: https_fn.Request) -> https_fn.Response:
    """main func"""
    with app.request_context(req.environ):
        return app.full_dispatch_request()
