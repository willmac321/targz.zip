module.exports = {
	env: { browser: true, es2020: true },
	extends: [
		'eslint:recommended',
		'xo',
		'plugin:react/recommended',
		'plugin:react-hooks/recommended',
		'plugin:@typescript-eslint/recommended',
		'prettier',
	],
	overrides: [
		{
			extends: ['xo-typescript'],
			files: ['*.ts', '*.tsx'],
		},
	],
	parser: '@typescript-eslint/parser',
	parserOptions: { ecmaVersion: 'latest', sourceType: 'module' },
	plugins: ['react', 'react-refresh'],
	rules: {
		'react-refresh/only-export-components': 'warn',
		// Disallow the `any` type.
		'@typescript-eslint/no-explicit-any': 'warn',
		'no-console': 'warn',
		'react/react-in-jsx-scope':'off',
		'react/jsx-uses-react':'off',
		'no-implicit-coercion': 'off',
	},
};
