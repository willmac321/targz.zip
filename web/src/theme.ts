const theme = {
	global: {
		font: {
			family: 'Inter, system-ui, Avenir, Helvetical, Arial, sans-serif',
			height: '1.5',
			weight: '400',
		},
	},

};

export {theme};
