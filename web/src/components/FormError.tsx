import {Box, Text} from 'grommet';
import {type ReactNode} from 'react';

const FormError = ({children}: {children: ReactNode}) => (
	<Box background="background-front" pad={'4px'} margin={{top: '16px'}} round="small">
		<Text color={'#ff2800'}>{children}</Text>
	</Box>
);

export default FormError;
