import {Text, FileInput as FileInputGrommet, Box, Image} from 'grommet';

import {type ChangeEvent, forwardRef, useEffect, useState} from 'react';
type FileInputType = {
	isLoading: boolean;
	onChange: (event?: ChangeEvent<HTMLInputElement>) => void;
};

const FileInputItem = ({file}: {file: File}) => {
	const [fileUrl, setFileUrl] = useState<string | undefined>();

	useEffect(() => {
		if (fileUrl) return;
		const reader = new FileReader();

		reader.onload = () => {
			if (reader.result) setFileUrl(reader.result as string);
		};

		reader.readAsDataURL(file);

		return () => {
			reader.abort();
		};
	}, [file, fileUrl]);

	return (
		<Box margin={{left: '16px', bottom: '16px'}}>
			<Box direction="row">
				{fileUrl && <Image height="120px" width="120px" src={fileUrl} />}
				<Box justify="start" gap="4px" margin={{top: '8px', left: '16px'}}>
					<Text>{file.name}</Text>
					<Text>{Math.trunc(file.size / 100_000) / 10} MB</Text>
				</Box>
			</Box>
		</Box>
	);
};

const FileInput = forwardRef<HTMLInputElement, FileInputType>(
	({isLoading, onChange}, ref): JSX.Element => (
		<FileInputGrommet
			ref={ref}
			name="fileInput"
			disabled={isLoading}
			accept="image/png, image/jpeg, image/jpg, image/webp"
			multiple={{max: 5, aggregateThreshold: 10}}
			onChange={onChange}
			renderFile={(file: File) => <FileInputItem file={file} />}
		/>
	),
);
FileInput.displayName = 'FileInput';

export default FileInput;
