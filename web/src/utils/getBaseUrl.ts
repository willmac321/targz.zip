const getBaseUrl = (): string => {
	if (import.meta.env.DEV) {
		return `${import.meta.env.VITE_BASE_URL}/${import.meta.env.VITE_APPPATH}`;
	}

	// On prod since using rewrites in firebase hosting config, don't need the app path
	return `${import.meta.env.VITE_BASE_URL}`;
};

export default getBaseUrl;
