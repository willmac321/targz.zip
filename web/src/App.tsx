import {RouterProvider, createBrowserRouter} from 'react-router-dom';
import Home from './views/Home';
import Shaqulator from './views/Shaqulator';
import IgFormatter from './views/IgFormat';
import {Grommet} from 'grommet';
import MediaPlayer from './views/MediaPlayer';
import {theme} from './theme';

function App() {
	const router = createBrowserRouter([
		{path: '/', element: <Home />},
		{path: 'shaqulator', element: <Shaqulator />},
		{path: 'igframe', element: <IgFormatter />},
		{path: 'hey', element: <MediaPlayer />},
	]);
	return (
		<Grommet full theme={theme} themeMode="dark" background="dark-1">
			<RouterProvider router={router} />
		</Grommet>
	);
}

export default App;
