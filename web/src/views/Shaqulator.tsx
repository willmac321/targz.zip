import {
	Page,
	PageContent,
	Box,
	FormField,
	Form,
	TextInput,
	Heading,
	Button,
	Text,
	RadioButtonGroup,
	Grid,
	Spinner,
	Image,
	ResponsiveContext,
} from 'grommet';
import {
	Favorite,
	IceCream,
	Cut,
	Key,
	Paint,
	Run,
	Sun,
	Tools,
	TreeOption,
	Yoga,
	Anchor,
	Car,
	Currency,
} from 'grommet-icons';
import styled from 'styled-components';
import {Helmet} from 'react-helmet';
import {useState, useRef, useContext} from 'react';
import shaq from '../assets/shaq.png';

const icons = [
	<Favorite key="1" />,
	<IceCream key="2" />,
	<Cut key="3" />,
	<Key key="4" />,
	<Paint key="5" />,
	<Run key="6" />,
	<Sun key="7" />,
	<Tools key="8" />,
	<TreeOption key="9" />,
	<Yoga key="10" />,
	<Anchor key="11" />,
	<Car key="12" />,
	<Currency key="13" />,
];

const SlotSpinner = styled(Spinner)`
  animation-name: rotate-1;
  animation-timing-function: linear;
  animation-duration: 0.5s;
  animation-iteration-count: infinite;
  @keyframes rotate-1 {
    to {
      transform: rotate(360deg);
    }
  }
`;

const Fadebox = styled(Box)`
  --mask: linear-gradient(
      to left,
      rgba(68, 68, 68, 1) 0,
      rgba(68, 68, 68, 1) 75%,
      rgba(68, 68, 68, 0) 95%,
      rgba(68, 68, 68, 0) 0%
    )
    100% 50% / 100% 100% repeat-x;
  mask: var(--mask);
`;

const Slotbox = styled(Box)`
  animation-name: slot-1;
  animation-timing-function: ease-in-out;
  animation-duration: 2s;
  animation-iteration-count: infinite;
  @keyframes slot-1 {
    0% {
      transform: translateX(0px);
    }
    100% {
      transform: translateX(-500px);
    }
  }
		}
`;

const initValue: TypeValue = {units: 'imperial'};

type TypeValue = {
	units: string;
	feet?: string;
	inch?: string;
	meter?: string;
	shaq?: string;
};

const Shaqulator = (): JSX.Element => {
	const [value, setValue] = useState(initValue);

	const size = useContext(ResponsiveContext);
	const [myShaqs, setMyShaqs] = useState<number | undefined>(undefined);
	const [isLoading, setIsLoading] = useState(false);
	const timer = useRef<ReturnType<typeof window.setTimeout>>();
	const getNum = () => Math.round(Math.random() * 12);
	const [iconOne] = useState(getNum());

	const findMyShaq = (value: TypeValue): void => {
		setIsLoading(true);

		if (timer.current) {
			clearTimeout(timer.current);
		}

		timer.current = setTimeout(() => {
			setIsLoading(false);
			timer.current = undefined;
		}, 2000);

		const shaq = 7.0 * 12 + 1.0;
		if (value.units === 'imperial') {
			const f = value.feet && parseFloat(value.feet) > 0 ? parseFloat(value.feet) : 0;
			const i = value.inch && parseFloat(value.inch) > 0 ? parseFloat(value.inch) : 0;
			setMyShaqs((f * 12 + i) / shaq);
		}

		if (value.units === 'metric') {
			const m = value.meter && parseFloat(value.meter) > 0 ? parseFloat(value.meter) : 0;
			setMyShaqs(m / (shaq * 0.0254));
		}

		if (value.units === 'shaq') {
			const s = value.shaq && parseFloat(value.shaq) > 0 ? parseFloat(value.shaq) : 0;
			setMyShaqs(s ?? 0);
		}
	};

	return (
		<Page pad={{vertical: 'medium'}} kind="narrow" background="background-back">
			<Helmet>
				<link rel="icon" type="image/gif" href="/hamwalk.gif" />
				<title>Shaqulator</title>
			</Helmet>
			<PageContent background="background-front" pad="medium" round="medium" alignContent="center">
				<Box
					direction="column"
					margin={{vertical: 'medium'}}
					align={size !== 'small' ? undefined : 'center'}
				>
					<Heading margin="none">How many Shaqs tall am I?</Heading>
					<Text margin={{left: 'small', vertical: 'medium'}}>
            Save some time, find your Shaqs online.
					</Text>
				</Box>
				<Box
					direction={size !== 'small' ? 'row-reverse' : 'column'}
					align="center"
					justify="around"
				>
					<Box
						width="40%"
						margin={{vertical: 'auto'}}
						pad={size !== 'small' ? undefined : '16px'}
					>
						<Image style={{borderRadius: '16px'}} fit="cover" fill src={shaq} />
					</Box>
					<Form
						value={value}
						onChange={(nextValue) => {
							setValue(nextValue);
						}}
						onReset={() => {
							setValue(initValue);
						}}
						onSubmit={({value}) => {
							findMyShaq(value);
						}}
					>
						<RadioButtonGroup
							name="units"
							value={value.units}
							id="radio-unit-select"
							direction="row"
							margin={{bottom: 'medium'}}
							justify={size !== 'small' ? undefined : 'center'}
							pad={{top: '16px'}}
							options={[
								{
									id: 'imperial-units',
									label: 'Imperial',
									value: 'imperial',
								},
								{
									id: 'metric-units',
									label: 'Metric',
									value: 'metric',
								},
								{
									id: 'shaq-units',
									label: 'Shaqs',
									value: 'shaqs',
								},
							]}
						/>

						<Grid columns="xsmall" rows="xsmall" gap="medium" width="320px" pad={{top: '16px'}}>
							{value.units === 'imperial' && (
								<>
									<FormField
										label="Feet"
										id="number-feet-input-id"
										name="feet"
										onChange={(event) => {
											const v = {...value, feet: event.target.value};
											setValue(v);
										}}
										value={value.feet}
										component={TextInput}
									/>
									<FormField
										label="Inches"
										id="number-inch-input-id"
										name="inch"
										onChange={(event) => {
											const v = {...value, inch: event.target.value};
											setValue(v);
										}}
										value={value.inch}
										component={TextInput}
									/>
								</>
							)}
							{value.units === 'metric' && (
								<FormField
									name="metric"
									label="Meters"
									component={TextInput}
									id="number-metric-input-id"
									onChange={(event) => {
										const v = {...value, meter: event.target.value};
										setValue(v);
									}}
									value={value.meter}
								/>
							)}
							{value.units === 'shaqs' && (
								<FormField
									label="Shaqs"
									id="number-shaq-input-id"
									name="shaq"
									onChange={(event) => {
										const v = {...value, shaq: event.target.value};
										setValue(v);
									}}
									value={value.shaq}
									component={TextInput}
								/>
							)}
						</Grid>
						<Box height={'100px'} width="100%">
							{isLoading && (
								<Fadebox
									width="100%"
									direction="row"
									overflow="hidden"
									margin={{vertical: 'auto'}}
								>
									<Slotbox direction="row">
										{Array.from(new Array(13)).map((_, i) => (
											<SlotSpinner
												pad={{vertical: 'large'}}
												justify="center"
												key={`one_${iconOne + i}`}
											>
												{icons[(iconOne + i) % 13]}
											</SlotSpinner>
										))}
									</Slotbox>
								</Fadebox>
							)}
							{!isLoading && myShaqs && (
								<Box
									width="100%"
									alignSelf="end"
									margin={{vertical: 'auto'}}
									animation={'fadeIn'}
								>
									<Text weight="bold" size="24px" textAlign="center">
										{Math.round((myShaqs + Number.EPSILON) * 1000) / 1000} Shaqs
									</Text>
								</Box>
							)}
						</Box>
						<Box
							align="start"
							justify={size !== 'small' ? 'end' : 'center'}
							direction="row"
							gap="medium"
							pad={{vertical: 'medium'}}
							margin={{bottom: '2rem'}}
						>
							<Button type="submit" label="Shaq" primary />
							<Button type="reset" label="Reset" />
						</Box>
					</Form>
				</Box>
			</PageContent>
		</Page>
	);
};

export default Shaqulator;
