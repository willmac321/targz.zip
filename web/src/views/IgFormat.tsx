import {Helmet} from 'react-helmet';
import {
	Page,
	PageContent,
	Box,
	Heading,
	Button,
	Spinner,
	Stack,
	Layer,
	Text,
	Form,
	FormField,
	TextInput,
	RangeInput,
	Notification,
} from 'grommet';
import {useState, useRef, type ChangeEvent} from 'react';
import axios from 'axios';
import getBaseUrl from '../utils/getBaseUrl';
import FormError from '../components/FormError';
import FileInput from '../components/FileInput';

type FormatFormData = {
	max_size?: number | string;
	border_width?: number | string;
	frame_color?: string;
	background_color?: string;
};

const defaultFormatFormData = {
	max_size: 1280,
	border_width: 20,
	frame_color: '#FFFFFF',
	background_color: '#000000',
};

const IgFormatter = () => {
	const maxSize: number = import.meta.env.VITE_MAX_CONTENT ?? 75;
	const baseUrl = getBaseUrl();
	const fileInputRef = useRef<HTMLInputElement>(null);
	const [isUploaded, setIsUploaded] = useState(false);
	const [fileList, setFileList] = useState<FileList | undefined>();
	const [isLoading, setIsLoading] = useState(false);
	const [isReloadInput, setReloadInput] = useState(false);
	const [formatFormData, setFormatFormData] = useState<FormatFormData>(defaultFormatFormData);
	const [showToast, setShowToast] = useState(false);

	const isOverMaxFileLimit = fileList
		? Array.from(fileList).reduce((acc, curr) => acc + curr.size, 0) / 1000 / 1000 > maxSize
		: false;

	const reset = async (fileList: FileList): Promise<void> => {
		setIsLoading(true);
		const url = `${baseUrl}/api/removeImages`;

		const fn = [];
		for (const file of fileList) {
			if (file) fn.push(file.name);
		}

		if (fn.length > 0) {
			try {
				await axios<string[]>({
					method: 'DELETE',
					params: {files: fn},
					url,
					headers: {'Content-Type': 'application/json'},
				});
			} catch (err) {
				setShowToast(true);
			}
		}

		setReloadInput(true);
		setFormatFormData(defaultFormatFormData);
		setFileList(undefined);
		setIsLoading(false);
		setIsUploaded(false);
		setReloadInput(false);
	};

	const handleSetFileList = (event?: ChangeEvent<HTMLInputElement>): void => {
		if (!event) setFileList(undefined);
		if (event?.target?.files?.length === 0) setFileList(undefined);

		setFileList(event?.target?.files ?? undefined);
	};

	const handleUpload = async (files: FileList | undefined): Promise<void> => {
		if (!files?.length) return;

		setIsLoading(true);
		const formData = new FormData();

		for (const file of Array.from(files)) {
			if (file) formData.append('file', file);
		}

		const url = `${baseUrl}/api/processImages`;

		try {
			await axios<string[]>({
				method: 'POST',
				url,
				data: formData,
				headers: {'Content-Type': 'multipart/form-data'},
			});
			setIsUploaded(true);
		} catch (err) {
			setShowToast(true);
		} finally {
			setIsLoading(false);
		}
	};

	const handleFormat = async (
		files: FileList | undefined,
		formatFormData: FormatFormData,
	): Promise<void> => {
		if (!files) return;
		setIsLoading(true);
		const url = `${baseUrl}/api/formatImages`;

		const fn = [];
		for (const file of files) {
			if (file) fn.push(file.name);
		}

		const formData = {
			...formatFormData,
			files: fn,
		};

		try {
			const res = await axios<string[]>({
				method: 'POST',
				url,
				data: formData,
				headers: {'Content-Type': 'application/json'},
			});
			if (res?.data?.length > 0 && res.status === 200) {
				const {data} = res;
				data.forEach(async (d) => {
					try {
						const response = await axios.get(d[1], {
							responseType: 'blob',
						});

						const url = window.URL.createObjectURL(new Blob([response.data]));
						const link = document.createElement('a');
						link.href = url;
						link.setAttribute('download', d[0]);
						document.body.appendChild(link);
						link.click();
						link.remove();
					} catch (error) {
						console.error('Error downloading file:', error);
					}
				});
			}
		} catch (err) {
			setShowToast(true);
		}

		setFormatFormData(defaultFormatFormData);
		setFileList(undefined);
		setIsUploaded(false);
		setIsLoading(false);
	};

	return (
		<Page
			pad={{vertical: 'medium'}}
			kind="narrow"
			height="100vh"
			background={{
				color: 'background-back',
				size: 'contain',
				repeat: 'repeat',
				image: 'url(aesthetic.jpg)',
				opacity: true,
			}}
		>
			<Helmet>
				<link rel="icon" type="image/gif" href="/hamwalk.gif" />
				<title>IG ImageFrame</title>
			</Helmet>
			<PageContent alignContent="center" overflow={'auto'} width={'100%'}>
				<Box width={'525px'} alignSelf="center" alignContent="center">
					<Stack anchor="center">
						<Box>
							<Box
								direction="column"
								margin={{vertical: 'medium'}}
								align={'center'}
								pad="medium"
								round="medium"
								background={{color: 'background-front', opacity: true}}
							>
								<Heading margin="none">~ ~ W O W ~ ~</Heading>
							</Box>
							<Box
								direction="column"
								margin={{vertical: 'medium'}}
								align={'center'}
								pad="medium"
								round="medium"
								background={{color: 'background-front', opacity: true}}
							>
								{!isUploaded && (
									<>
										<Box background="background-front">
											{!isReloadInput && (
												<FileInput
													ref={fileInputRef}
													isLoading={isLoading}
													onChange={handleSetFileList}
												/>
											)}
										</Box>
										{fileList && fileList?.length > 5 && (
											<FormError>Sorry, too many files. Remove some!</FormError>
										)}
										{isOverMaxFileLimit && (
											<FormError>Sorry, too much file. Remove a few to reduce file size.</FormError>
										)}
										<Box margin={{vertical: 'medium'}}>
											<Button
												primary
												disabled={
													isLoading || !fileList || fileList?.length <= 0 || fileList?.length > 5
												}
												label="upload"
												type="button"
												onClick={async () => {
													setReloadInput(true);
													await handleUpload(fileList);
													if (fileInputRef?.current?.files) {
														fileInputRef.current.files = null;
													}

													setReloadInput(false);
												}}
											/>
										</Box>
									</>
								)}
								{fileList?.length && fileList.length > 0 && isUploaded && (
									<>
										<Box
											margin="medium"
											align={'center'}
											pad={{vertical: 'medium', horizontal: 'large'}}
											round="medium"
											background={{color: 'background-back', opacity: 'strong'}}
										>
											{Array.from(fileList).map((file) => (
												<Text key={file.name}>{file.name}</Text>
											))}
										</Box>
										<Form
											value={formatFormData}
											onChange={(nextValue) => {
												setFormatFormData(nextValue);
											}}
											onReset={async () => {
												await reset(fileList);
											}}
											onSubmit={async () => {
												await handleFormat(fileList, formatFormData);
											}}
										>
											<FormField
												name="border_width"
												htmlFor="numeric-input-border"
												label="Border Width (px)"
											>
												<Text alignSelf="end">{formatFormData.border_width} px</Text>
												<RangeInput
													min="0"
													max="200"
													step={10}
													value={formatFormData.border_width}
													id="numeric-input-border"
													name="border_width"
													type="number"
												/>
											</FormField>
											<FormField
												name="max_size"
												htmlFor="numeric-input-max_size"
												label="Maximum Width or Height (px)"
											>
												<Text alignSelf="end">{formatFormData.max_size} px</Text>
												<RangeInput
													min="400"
													max="2560"
													step={20}
													value={formatFormData.max_size}
													id="numeric-input-max_size"
													name="max_size"
													type="number"
												/>
											</FormField>
											<FormField name="frame_color" htmlFor="frame-color-input" label="Frame Color">
												<Text alignSelf="end" margin={{horizontal: 'small'}}>
													{formatFormData.frame_color}
												</Text>
												<TextInput
													id="frame-color-input"
													name="frame_color"
													type="color"
													textAlign="end"
													style={{
														width: '120px',
														height: '48px',
														textAlign: 'end',
														float: 'right',
													}}
												/>
											</FormField>
											<FormField
												name="background_color"
												htmlFor="bg-color-input"
												label="Background Color"
											>
												<Text alignSelf="end" margin={{horizontal: 'small'}}>
													{formatFormData.background_color}
												</Text>
												<TextInput
													id="bg-color-input"
													name="background_color"
													type="color"
													textAlign="end"
													style={{
														width: '120px',
														height: '48px',
														textAlign: 'end',
														float: 'right',
													}}
												/>
											</FormField>
											<Box
												justify="end"
												width="100%"
												direction="row"
												gap="small"
												margin={{vertical: 'medium'}}
											>
												<Button disabled={isLoading} primary label="submit" type="submit" />
												<Button disabled={isLoading} label="reset" type="reset" />
											</Box>
										</Form>
									</>
								)}
							</Box>
						</Box>
						{isLoading && (
							<Layer full background={{color: 'unset'}}>
								<Box height="78%" margin="auto" align="center">
									<Spinner
										size="xlarge"
										background={{color: 'background-back', opacity: 'weak'}}
									/>
								</Box>
							</Layer>
						)}
						{showToast && (
							<Notification
								toast
								title="Error"
								message="There was an error fulfilling your request, please try again."
								onClose={() => {
									setShowToast(false);
								}}
							/>
						)}
					</Stack>
				</Box>
			</PageContent>
		</Page>
	);
};

export default IgFormatter;
