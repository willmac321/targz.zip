import {Layer, Spinner, Notification, Box, Heading, Page, PageContent, Video} from 'grommet';
import {useEffect, useState} from 'react';
import getBaseUrl from '../utils/getBaseUrl';
import axios from 'axios';
import {Helmet} from 'react-helmet';

const MediaPlayer = (): JSX.Element => {
	const baseUrl = getBaseUrl();

	const [isLoading, setIsLoading] = useState(false);
	const [videoUrl, setVideoUrl] = useState<string>();
	const [showToast, setShowToast] = useState(false);

	useEffect(() => {
		(async () => {
			setIsLoading(true);
			const url = `${baseUrl}/api/media`;

			try {
				const rv = await axios<string>({
					method: 'GET',
					params: {file: 'ruA5dIS.mp4', directory: 'mediaPlayer'},
					url,
					headers: {'Content-Type': 'application/json'},
				});
				if (rv.status === 200 && rv.data.length > 0) {
					setVideoUrl(rv.data);
				}
			} catch (err) {
				setShowToast(true);
			} finally {
				setIsLoading(false);
			}
		})();
	}, [baseUrl]);

	return (
		<Page
			pad={{vertical: 'medium'}}
			kind="narrow"
			height="100%"
			background={{
				color: 'background-back',
			}}
		>
			<Helmet>
				<link rel="icon" type="image/gif" href="/hamwalk.gif" />
				<title>Play Video</title>
			</Helmet>
			<PageContent alignContent="center">
				<Box
					direction="column"
					margin={{vertical: 'medium'}}
					align={'center'}
					pad="medium"
					round="medium"
					background={{color: 'background-front', opacity: true}}
				>
					<Heading margin="none">check this out</Heading>
				</Box>
				{!isLoading && (
					<Video controls="over" fit="contain" preload="auto">
						<source key="video" type="video/mp4" src={videoUrl} />
					</Video>
				)}
				{isLoading && (
					<Layer full background={{color: 'unset'}}>
						<Box height="78%" margin="auto" align="center">
							<Spinner size="xlarge" background={{color: 'background-back', opacity: 'weak'}} />
						</Box>
					</Layer>
				)}
				{showToast && (
					<Notification
						toast
						title="Error"
						message="There was an error fulfilling your request, please try again."
						onClose={() => {
							setShowToast(false);
						}}
					/>
				)}
			</PageContent>
		</Page>
	);
};

export default MediaPlayer;
