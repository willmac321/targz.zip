import anin from '../assets/anin.gif';
import hamu from '../assets/hamu.gif';
import gerbil from '../assets/gerbil.gif';
import hamwalk from '../assets/hamwalk.gif';
import dedodedo from '../assets/dedodedo.wav';
import { Helmet } from 'react-helmet';
import { Page } from 'grommet';

function Home() {
  // UseEffect(() => {
  // 	const link = document.createElement('a');
  // 	link.href = 'potato.zip';
  // 	link.setAttribute('download', 'targz.zip');

  // 	document.body.appendChild(link);

  // 	link.click();

  // 	link.parentNode?.removeChild(link);
  // }, []);

  return (
    <Page background="white">
      <div>
        <Helmet>
          <link rel="icon" type="image/gif" href="/hamwalk.gif" />
          <title>I&#39;m having fun on the internet</title>
        </Helmet>
        <center>
          <audio id="dedodedo" controls loop>
            <source src={dedodedo} type="audio/mpeg" />
            Your browser does not support the audio element.
          </audio>
        </center>
        <center>
          <h1>{'THE HAMSTER DANCE'}</h1>
          <h3>dance the night away</h3>
        </center>
        <center>
          {Array.from({ length: 4 }).map(() => (
            <>
              {Array.from({ length: 5 }).map(() => (
                <img src={anin} alt="anin.gif" />
              ))}
              {Array.from({ length: 12 }).map(() => (
                <img src={hamu} alt="hamu.gif" />
              ))}
              {Array.from({ length: 5 }).map(() => (
                <img src={anin} alt="anin.gif" />
              ))}
              {Array.from({ length: 12 }).map(() => (
                <img src={gerbil} alt="gerbil.gif" />
              ))}
              {Array.from({ length: 5 }).map(() => (
                <img src={anin} alt="anin.gif" />
              ))}
              {Array.from({ length: 14 }).map(() => (
                <img src={hamwalk} alt="hamwalk.gif" />
              ))}
              {Array.from({ length: 5 }).map(() => (
                <img src={anin} alt="anin.gif" />
              ))}
            </>
          ))}
        </center>
      </div>
    </Page>
  );
}

export default Home;
